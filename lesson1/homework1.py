#Задача 1: Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
print(">>>task 1 \n")

first = 10
second = 30

result = first + second
print(f'{first} + {second} = ', result)

result = first - second
print(f'{first} - {second} = ', result)

result = first * second
print(f'{first} * {second} = ', result)

result = first / second
print(f'{first} / {second} = ', result)

result = first ** second
print(f'{first} ** {second} = ', result)

result = first // second
print(f'{first} // {second} = ', result)

result = first % second
print(f'{first} % {second} = ', result)


#Задача 2: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.

print("\n>>>task 2 \n")

mybool = first > second
print(f'Is {first} > {second}? It\'s', mybool)

mybool = first >= second
print(f'Is {first} >= {second}? It\'s', mybool)

mybool = first < second
print(f'Is {first} < {second}? It\'s', mybool)

mybool = first <= second
print(f'Is {first} <= {second}? It\'s', mybool)

mybool = first == second
print(f'Is {first} = {second}? It\'s', mybool)

mybool = first != second
print(f'Is {first} not = {second}? It\'s', mybool)

#Задача 3: Створіть змінну - результат конкатенації строк "Hello " та "world!"

print ("\n>>>task 3\n")

word1 = "Hello "
word2 = "world!"
phrase = word1 + word2
print(phrase)