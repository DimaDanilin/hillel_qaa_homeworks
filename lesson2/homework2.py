#Задача 1. Сформуйте стрінг, в якому міститься інформація про певне слово.
# "Word [тут слово] has [тут довжина слова, отримайте з самого сдлва] letters",
# наприклад "Word 'Python' has 6 letters".
# Для отримання слова для аналізу скористайтеся константою або функцією input().

print('Task 1 \n')

word_input = input('Enter a word --> ').strip()

if len(word_input) == 0:
    print('Please, enter something')

else:
    word_no_space = word_input.replace(' ', '') #добавив видалення пробілу, бо з вимог завдвння не ясно чи потрібно рахувати пробіл
    if len(word_no_space) == 1:
        print(f'Word "{word_input}" has {len(word_no_space)} letter')
    else:
        print(f'Word "{word_input}" has {len(word_no_space)} letters')

#Код якщо потрібно рахувати всі символи, пробіл включно
# else:
#     if len(word_input) == 1:
#         print(f'Word "{word_input}" has {len(word_input)} letter')
#     else:
#         print(f'Word "{word_input}" has {len(word_input)} letters')



# Задача 2. Напишіть программу "Касир в кінотеватрі", яка попросіть користувача ввести свсвій вік (можно використати константу або функцію input(), на екран має бути виведено лише одне повідомлення, також подумайте над варіантами, коли введені невірні дані).
# якщо користувачу менше 7 - вивести повідомлення"Де твої батьки?"
# якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
# якщо користувачу більше 65 - вивести повідомлення"Покажіть пенсійне посвідчення!"
# якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
# у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"

print('\nTask 2\n')

customer_age = input('Введіть Ваш повний вік --> ').strip()
# customer_age = customer_age_input.strip()

if customer_age.isalpha() or not customer_age.isdigit() or int(customer_age) == 0 or len(customer_age) == 0:
    print('Невірно введені дані. \nВік повин бути в цифровому форматі та не містити пробілів')

elif '7' in customer_age and int(customer_age) <= 120:
    print('Вам сьогодні пощастить!')

else:
    age = int(customer_age)
    if age < 7:         # В умовах не зазначено чи повинно 7 входити в діапозон
        print('Де твої батьки?')

    elif age >= 7 and age < 16:  # В умовах не зазначено чи повинно 16 входити в діапозон
        print('Це фільм для дорослих!')

    elif age > 65 and age <= 120:    # В умовах не зазначено чи повинно 65 входити в діапозон
        print('Покажіть пенсійне посвідчення!')

    else:
        print('А білетів вже немає!')